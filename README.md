# 自媒体平台解决方案

#### 简介

自媒体平台解决方案是针对自媒体创业者打造的一款资讯平台解决方案，属于UGC和PGC产品，用户可以分享自己的动态，类似于朋友圈那样，内容生产者，这里指的是专业从事创作内容的个人或者团队发布他们的内容。普通用户可以通过该平台阅读自己喜欢和关注的内容。

#### 特点

-  **适用领域：** 校园资讯，音乐爱好者，宠物联盟，旅游户外等等垂直领域；
-  **适合人群：** 自主创作的团队或个人；
-  **支持平台：** 微信小程序，QQ小程序，PC浏览器，手机浏览器。

#### 案例
  
原创校园是一款专注大学生的内容资讯平台。
大家不仅可以阅读到校园热点，学习资料，演出活动，深度美文，恋爱故事，书法绘画等优秀文章，还可以发现校园新鲜事，穷游攻略，二手闲置，兼职家教，求助信息，周边租房，寻物启事，失物招领等生活信息。

#### 功能

 **1. 微信小程序截图** 
  
<img src="https://images.gitee.com/uploads/images/2019/0925/235246_14120761_372436.png" title="文章首页" width="40%" height="40%" border="1" />
<img src="https://images.gitee.com/uploads/images/2019/0925/235633_3e591921_372436.png" title="文章详情" width="40%" height="40%" />

---------------------------------------

<img src="https://images.gitee.com/uploads/images/2019/0925/235842_1fe5771c_372436.png" title="动态列表" width="40%" height="40%" />
<img src="https://images.gitee.com/uploads/images/2019/0925/235918_423d9b2d_372436.png" title="我的页面" width="40%" height="40%" />

---------------------------------------

 **2. QQ小程序截图** 

<img src="https://images.gitee.com/uploads/images/2019/0926/002510_13d1267a_372436.png" title="文章首页" width="40%" height="40%" />
<img src="https://images.gitee.com/uploads/images/2019/0926/002622_2f258d28_372436.png" title="文章详情" width="40%" height="40%" />

---------------------------------------

<img src="https://images.gitee.com/uploads/images/2019/0926/002646_103617d6_372436.png" title="动态列表" width="40%" height="40%" />
<img src="https://images.gitee.com/uploads/images/2019/0926/002714_20ec6e27_372436.png" title="我的页面" width="40%" height="40%" />

---------------------------------------

 **3. 创作者微信小程序截图** 

<img src="https://images.gitee.com/uploads/images/2019/0926/003043_781a6009_372436.png" title="登录页面" width="40%" height="40%" />
<img src="https://images.gitee.com/uploads/images/2019/0926/003236_1b0d78ad_372436.png" title="文章列表" width="40%" height="40%" />

---------------------------------------

<img src="https://images.gitee.com/uploads/images/2019/0926/003321_b5903fd5_372436.png" title="文章发布" width="40%" height="40%" />
<img src="https://images.gitee.com/uploads/images/2019/0926/003405_78707d92_372436.png" title="我的页面" width="40%" height="40%" />

---------------------------------------

 **4. 原创校园PC版截图** 

<img src="https://images.gitee.com/uploads/images/2019/0926/225944_1b295f2c_372436.png" title="文章列表" width="100%" height="100%" />
<img src="https://images.gitee.com/uploads/images/2019/0926/230015_23d130e9_372436.png" title="文章详情" width="100%" height="100%" />

---------------------------------------

**5. 创作者PC版截图** 

<img src="https://images.gitee.com/uploads/images/2019/0926/232419_7c317808_372436.png" title="综合数据" width="100%" height="100%" />
<img src="https://images.gitee.com/uploads/images/2019/0926/232715_15768e29_372436.png" title="文章发布" width="100%" height="100%" />
<img src="https://images.gitee.com/uploads/images/2019/0926/232446_b9709fa0_372436.png" title="文章列表" width="100%" height="100%" />
<img src="https://images.gitee.com/uploads/images/2019/0926/232607_9cb9c37b_372436.png" title="粉丝列表" width="100%" height="100%" />
<img src="https://images.gitee.com/uploads/images/2019/0926/232636_e6b48007_372436.png" title="账号管理" width="100%" height="100%" />

---------------------------------------

#### 谈谈未来

- 如果有对《原创校园》资讯平台有兴趣了，可以联系我们，一起合伙做好这个项目；
- 如果有伙伴想拿这套系统做创业的，也欢迎联系我们，我们一起沟通如何做好它。

#### 扫码体验

<img src="https://images.gitee.com/uploads/images/2019/0926/234234_a83744b3_372436.png" title="微信小程序" width="30%" height="30%" />
<img src="https://images.gitee.com/uploads/images/2019/0926/234039_a6dcee55_372436.png" title="QQ小程序" width="30%" height="30%" />
<img src="https://images.gitee.com/uploads/images/2019/0926/234346_5e5ce6da_372436.jpeg" title="创作者小程序" width="30%" height="30%" />

---------------------------------------

#### 联系我们

<img src="https://images.gitee.com/uploads/images/2019/0926/234828_db7f2011_372436.jpeg" title="微信小程序" width="40%" height="40%" />

---------------------------------------